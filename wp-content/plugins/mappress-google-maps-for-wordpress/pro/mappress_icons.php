<?php
class Mappress_Icons {
	static  $icons_dir,
			$icons_url,
			$standard_icons_url,
			$user_icons = array(),
			$standard_icons = array ("blue-dot", "ltblue-dot", "green-dot", "pink-dot", "purple-dot", "red-dot", "yellow-dot", "blue", "green", "lightblue", "pink", "purple", "red", "yellow", "blue-pushpin", "grn-pushpin", "ltblu-pushpin", "pink-pushpin", "purple-pushpin", "red-pushpin", "ylw-pushpin", "bar", "coffeehouse", "man", "wheel_chair_accessible", "woman", "restaurant", "snack_bar", "parkinglot", "bus", "cabs", "ferry", "helicopter", "plane", "rail", "subway", "tram", "truck", "info", "info_circle", "rainy", "sailing", "ski", "snowflake_simple", "swimming", "water", "fishing", "flag", "marina", "campfire", "campground", "cycling", "golfer", "hiker", "horsebackriding", "motorcycling", "picnic", "POI", "rangerstation", "sportvenue", "toilets", "trail", "tree", "arts", "conveniencestore", "dollar", "electronics", "euro", "gas", "grocerystore", "homegardenbusiness", "mechanic", "movies", "realestate", "salon", "shopping", "yen", "caution", "earthquake", "fallingrocks", "firedept", "hospitals", "lodging", "phone", "partly_cloudy", "police", "postoffice-us", "sunny", "volcano", "camera", "webcam", "iimm1-blue", "iimm1-green", "iimm1-orange", "iimm1-red", "iimm2-blue", "iimm2-green", "iimm2-orange", "iimm2-red", "poly", "kml");

	static function register() {
		add_action('wp_ajax_mapp_get_icon_picker', array(__CLASS__, 'ajax_get_icon_picker'));

		// Create directories
		$upload = wp_upload_dir();
		$basedir = $upload['basedir'] . "/mappress";
		$baseurl = $upload['baseurl'] . "/mappress";
		wp_mkdir_p($basedir);
		wp_mkdir_p($basedir . "/icons");
		self::$icons_dir = $basedir . "/icons/";
		self::$icons_url = $baseurl . "/icons/";
		self::$standard_icons_url = Mappress::$baseurl . '/pro/standard_icons/';
	}

	static function ajax_get_icon_picker() {
		ob_start();

		// Read user icons
		$user_icons = '';
		$files = @scandir(self::$icons_dir);
		if ($files) {
			natcasesort($files);
			foreach($files as $file) {
				if ( !stristr($file, '.shadow') && ( stristr($file, '.png') || stristr($file, '.gif')) ) {
					$url = self::$icons_url . $file;
					$user_icons .= "<img class='mapp-icon' data-mapp-iconid='$file' src='$url' alt='$file' title='$file' />";
				}
			}
		}

		// Standard icons
		$standard_icons = '';
		foreach(self::$standard_icons as $i => $icon) {
			$style = 'background-position: ' . ($i * -32) . 'px 0px;';
			$standard_icons .= "<span data-mapp-iconid='$icon' class='mapp-icon-sprite' style='$style' alt='$icon' title='$icon'></span>";
		}

		$html = "
			<span class='mapp-close' data-mapp-action='cancel'></span>
			<div class='mapp-iconpicker-wrapper'>
				<div>$user_icons</div><div>$standard_icons</div>
			</div>
			<div class='mapp-iconpicker-toolbar'>
				<input class='button' data-mapp-iconid='' type='button' value='" . __('Use default icon') . "' />
				<input class='button data-mapp-action='cancel' type='button' value='" . __('Cancel') . "' />
			</div>
		";

		Mappress::ajax_response('OK', $html);
	}

	static function spritesheet() {
		$sheet_width = count(self::$standard_icons) * 32;
		$sheet_height = 32;

		// Create empty placeholder image
		$sheet = imagecreatetruecolor($sheet_width, $sheet_height);
		imagealphablending($sheet, false);
		imagesavealpha($sheet, true);

		$offset = 0;
		$css = array();
		foreach (self::$standard_icons as $iconid) {
			$file = Mappress::$basedir . "/pro/standard_icons/$iconid.png";
			if (!file_exists($file))
				continue;

			$sprite = imagecreatefrompng($file);
			list($width, $height) = getimagesize($file);

			for ($y = 0; $y < $height; $y++) {
				for ($x = 0; $x < $width; $x++) {
					$color = imagecolorat($sprite, $x, $y);
					imagesetpixel($sheet, $offset + $x, $y, $color);
				}
			}
			$offset += 32;
		}

		// Save the spritesheet
		imagepng($sheet, Mappress::$basedir . '/images/icons.png');
	}

	/**
	* Get an icon image for output in the poi list
	*
	* @param mixed $icon
	* @param mixed $args
	*/
	static function get($icon) {
		$icon = (!empty($icon)) ? $icon : Mappress::$options->defaultIcon;
		if (empty($icon))
			$icon = 'red-dot';
		$url = ( stristr($icon, '.png') || stristr($icon, '.gif')) ? self::$icons_url . $icon : self::$standard_icons_url . $icon . '.png';
		return "<img class='mapp-icon' src='$url' />";
	}
}
?>