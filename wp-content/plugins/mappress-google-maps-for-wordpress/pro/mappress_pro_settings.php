<?php
class Mappress_Pro_Settings extends Mappress_Settings {
	function __construct() {
		parent::__construct();
	}

	static function register() {
		add_action('wp_ajax_mapp_get_rule_values', array(__CLASS__, 'ajax_get_rule_values'));
	}

	function admin_init() {
		parent::admin_init();

		add_settings_field('poiList', __('POI list', 'mappress'), array($this, 'set_poi_list'), 'mappress', 'appearance_settings');
		add_settings_field('dataTables', __('Use DataTables', 'mappress'), array($this, 'set_data_tables'), 'mappress', 'appearance_settings');

		add_settings_field('iwType', __('InfoWindow type', 'mappress'), array($this, 'set_iw_type'), 'mappress', 'poi_settings');

		add_settings_field('defaultIcon', __('Default icon', 'mappress'), array($this, 'set_default_icon'), 'mappress', 'icons_settings');
		add_settings_field('customIconsDir', __('Custom icons directory', 'mappress'), array($this, 'set_custom_icons_dir'), 'mappress', 'icons_settings');
		add_settings_field('iconScale', __('Icon scaling', 'mappress'), array($this, 'set_icon_scale'), 'mappress', 'icons_settings');

		add_settings_field('mashupTitle', __('Mashup POI title', 'mappress'), array($this, 'set_mashup_title'), 'mappress', 'mashup_settings');
		add_settings_field('mashupBody', __('Mashup POI body', 'mappress'), array($this, 'set_mashup_body'), 'mappress', 'mashup_settings');
		add_settings_field('mashupClick', __('Mashup POI click', 'mappress'), array($this, 'set_mashup_click'), 'mappress', 'mashup_settings');
		add_settings_field('mashupLink', __('Link title', 'mappress'), array($this, 'set_mashup_link'), 'mappress', 'mashup_settings');
		add_settings_field('thumbs', __('Mashup Thumbnails', 'mappress'), array($this, 'set_thumbs'), 'mappress', 'mashup_settings');
		add_settings_field('thumbSize', __('Thumbnail Size', 'mappress'), array($this, 'set_thumb_size'), 'mappress', 'mashup_settings');
		add_settings_field('autoicons', __('Automatic icons', 'mappress'), array($this, 'set_autoicons'), 'mappress', 'mashup_settings');

		add_settings_field('styles', __('Styled maps', 'mappress'), array($this, 'set_styles'), 'mappress', 'styled_maps_settings');
		add_settings_field('style', __('Default style', 'mappress'), array($this, 'set_style'), 'mappress', 'styled_maps_settings');

		add_settings_field('geocoders', __('Geocoder(s)', 'mappress'), array($this, 'set_geocoders'), 'mappress', 'geocoding_settings');
		add_settings_field('metaKeys', __('Geocoding fields', 'mappress'), array($this, 'set_meta_keys'), 'mappress', 'geocoding_settings');

		add_settings_field('api_key', __('API key (optional)', 'mappress'), array($this, 'set_api_key'), 'mappress', 'misc_settings');
		add_settings_field('forceresize', __('Force resize', 'mappress'), array($this, 'set_force_resize'), 'mappress', 'misc_settings');
	}

	function set_options($input) {
		// Remove blank entries from address metakeys
		$keys = array();
		foreach($input['metaKeyAddress'] as $key) {
			if (!empty($key))
				$keys[] = $key;
		}
		$input['metaKeyAddress'] = $keys;

		// Cast widths/heights to integer; if either dimension is missing, set to empty array
		foreach(array('iconScale') as $dimension) {
			$size = (isset($input[$dimension])) ? $input[$dimension] : array(0,0);
			$input[$dimension] = ((int) $size[0] && (int) $size[1]) ? array((int) $size[0], (int) $size[1]) : array('', '');
		}

		// Cast thumbnail width/height to integer
		$input['thumbWidth'] = (int) $input['thumbWidth'];
		$input['thumbHeight'] = (int) $input['thumbHeight'];

		// Conbine arrays
		$this->combine($input['autoicons'], false, 2);

		// Convert styles to associative array
		$input['styles'] = array();
		if (isset($input['style_names']) && !empty($input['style_names'])) {
			foreach($input['style_names'] as $i => $name) {
				$json = $input['style_jsons'][$i];
				if (empty($name) || empty($json))
					continue;
				$input['styles'][$name] = $json;
			}
		}

		// If resize was clicked then resize ALL maps
		if (isset($_POST['force_resize']) && $_POST['resize_from']['width'] && $_POST['resize_from']['height']
		&& $_POST['resize_to']['width'] && $_POST['resize_to']['height']) {
			$maps = Mappress_Map::get_list();
			foreach ($maps as $map) {
				if ($map->width == $_POST['resize_from']['width'] && $map->height == $_POST['resize_from']['height']) {
					$map->width = $_POST['resize_to']['width'];
					$map->height = $_POST['resize_to']['height'];
					$map->save($postid);
				}
			}
		}

		return parent::set_options($input);
	}

	/**
	* Convert related POST arrays into a single associative or indexed array.
	*
	* If the array has a column 'id' then associative is returned.
	* The other values may be a single field or an array (id=>value and id => [col1, col2...])
	*
	* @param mixed $a - array to convert
	*/
	function combine(&$a, $assoc = true, $keys = 1) {
		// Empty array
		if (empty($a))
			return $a = array();

		$result = array();
		$cols = array_keys($a);

		for ($i = 0; $i < count($a[$cols[0]]); $i++) {
			$id = ($assoc) ? $a[$cols[0]][$i] : $i;

			$row = array();
			foreach($cols as $j => $col) {
				// If a key column is empty, skip the whole row
				$value = trim($a[$col][$i]);
				if (empty($value) && $j < $keys)
					continue 2;

				// Don't store first (ID) column for assoc
				if ($assoc && !$j)
					continue;

				if (count($cols) > 2)
					$row[$col] = $value;
				else
					$row = $value;
			}
			$result[$id] = $row;
		}
		$a = $result;
	}


	function set_autoicons() {
		$name = 'mappress_options[autoicons]';
		$keys = self::get_rule_keys();
		$autoicons = ($this->options->autoicons) ? $this->options->autoicons : array();
		$autoicons += array('' => null);

		foreach($autoicons as $autoicon) {
			$rule = (object) wp_parse_args($autoicon, array('key' => null, 'value' => null, 'iconid' => null));
			$values = self::get_rule_values($rule->key);

			$rows[] = array(
				// Class rule-key/rule-values identifies dependent selects
				Mappress_Controls::select("{$name}[key][]", $keys, $rule->key, array('none' => true, 'class' => 'mapp-rule-key')),
				Mappress_Controls::select("{$name}[value][]", $values, $rule->value, array('class' => 'mapp-rule-values', 'none' => true)),
				Mappress_Controls::icon_picker("{$name}[iconid][]", $rule->iconid)
			);
		}

		$headers = array(__('Key', 'mappress'), __('Value', 'mappress'), __('Icon', 'mappress'));
		echo Mappress_Controls::grid($headers, $rows, array('sortable' => true));
	}

	function set_poi_list() {
		echo self::checkbox($this->options->poiList, 'mappress_options[poiList]', __("Show a list of POIs under each map", 'mappress'));
	}

	function set_data_tables() {
		$link = "<a href='http://www.datatables.net'>DataTable</a>";
		echo self::checkbox($this->options->dataTables, 'mappress_options[dataTables]', sprintf(__("Show the POI list as a sortable %s", 'mappress'), $link));
	}

	function set_mashup_title() {
		$title_types = array('poi' => __('POI title', 'mappress'), 'post' => __('Post title', 'mappress'));
		echo self::radio($title_types, $this->options->mashupTitle, 'mappress_options[mashupTitle]');
	}

	function set_mashup_body() {
		$body_types = array('poi' => __('POI body', 'mappress'), 'address' => __('Address', 'mappress'), 'post' => __('Post excerpt', 'mappress'));
		echo self::radio($body_types, $this->options->mashupBody, 'mappress_options[mashupBody]');
	}

	function set_mashup_link() {
		echo self::checkbox($this->options->mashupLink, 'mappress_options[mashupLink]', __("Link POI titles to the underlying post", 'mappress'));
	}

	function set_mashup_click() {
		$types = array('poi' => __('Open the POI', 'mappress'), 'post' => __('Go directly to the post'));
		echo self::radio($types, $this->options->mashupClick, 'mappress_options[mashupClick]');
	}

	function set_icon_scale() {
		$name = 'mappress_options[iconScale]';
		$scale = ($this->options->iconScale) ? $this->options->iconScale : array('', '');
		echo Mappress_Controls::input("{$name}[0]", $scale[0], array('maxlength' => 3, 'size' => 3));
		echo ' X ';
		echo Mappress_Controls::input("{$name}[1]", $scale[1], array('maxlength' => 3, 'size' => 3));
		echo ' (px)';
	}

	function set_iw_type() {
		$iw_types = array(
			'iw' => __('Google InfoWindow', 'mappress'),
			'ib' => __('InfoBox', 'mappress')
		);
		echo self::radio($iw_types, $this->options->iwType, 'mappress_options[iwType]');
	}

	function set_thumbs() {
		echo self::checkbox($this->options->thumbs, 'mappress_options[thumbs]', __("Show featured image thumbnails in mashup POIs", 'mappress'));
	}

	function set_thumb_size() {
		// Note: WP doesn't return dimensions, just the size names - ticket is > 6 months old now: http://core.trac.wordpress.org/ticket/18947
		$sizes = get_intermediate_image_sizes();
		$sizes = array_combine(array_values($sizes), array_values($sizes));

		_e("Use existing size: ", 'mappress');
		echo self::dropdown($sizes, $this->options->thumbSize, 'mappress_options[thumbSize]', array('none' => true));

		echo "<br/>" . __("or resize to (px): ", 'mappress');
		echo "<input name='mappress_options[thumbWidth]' size='3' maxlength='3' value='" . $this->options->thumbWidth . "'/> X ";
		echo "<input name='mappress_options[thumbHeight]' size='3' maxlength='3' value='" . $this->options->thumbHeight . "'/>";
	}

	function set_default_icon() {
		echo Mappress_Controls::icon_picker('mappress_options[defaultIcon]', $this->options->defaultIcon);
	}

	function set_custom_icons_dir() {
		echo "<code>" . Mappress_Icons::$icons_dir . "</code>";
	}

	function set_styles() {
		$styles_link = "<a href='https://developers.google.com/maps/documentation/javascript/styling' target='_blank'>" . __("styled maps", 'mappress') . "</a>";
		$wizard_link = "<a href='http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html' target='_blank'>" . __('styled maps wizard', 'mappress') . "</a>";

		echo sprintf(__("Enter JSON for %s from Google's %s", 'mappress'), $styles_link, $wizard_link) . ": <br/>";

		$styles = $this->options->styles;

		// Add a blank row if the table is empty
		if (empty($styles))
			$styles = array('' => '');

		$rows = array();
		$headers = array(__("Style name", 'mappress'), 'JSON', '');
		foreach($styles as $style_name => $style) {
			$rows[] = array(
				"<input type='text' size='20' name='mappress_options[style_names][]' value='$style_name' />",
				"<textarea rows='1' class='mapp-expand' name='mappress_options[style_jsons][]'>$style</textarea>"
			);
		}

		echo $this->table($headers, $rows, array('id' => 'mapp_styles'));
	}

	function set_style() {
		$styles = $this->options->styles;
		if (empty($styles)) {
			_e('No styles have been defined yet', 'mappress');
			return;
		}

		$style_names = array_combine(array_keys($styles), array_keys($styles));
		echo self::dropdown($style_names, $this->options->style, 'mappress_options[style]', array('none' => true));
	}

	function set_geocoders() {
		$labels = array(
			'google' => __('Google', 'mappress'),
			'nominatim' => __('Nominatim', 'mappress')
		);
		echo self::checkbox_list($this->options->geocoders, 'mappress_options[geocoders][]', $labels);
	}

	function set_meta_keys() {
		$meta_keys = self::get_meta_keys();
		$zooms = array_combine(range(1,19), range(1,19));

		$this->options->metaKeyAddress = array_pad($this->options->metaKeyAddress, 6, '');

		$fields = array(
			__('Address Line 1', 'mappress') => array($this->options->metaKeyAddress[0], 'mappress_options[metaKeyAddress][0]'),
			__('Address Line 2', 'mappress') => array($this->options->metaKeyAddress[1], 'mappress_options[metaKeyAddress][1]'),
			__('Address Line 3', 'mappress') => array($this->options->metaKeyAddress[2], 'mappress_options[metaKeyAddress][2]'),
			__('Address Line 4', 'mappress') => array($this->options->metaKeyAddress[3], 'mappress_options[metaKeyAddress][3]'),
			__('Address Line 5', 'mappress') => array($this->options->metaKeyAddress[4], 'mappress_options[metaKeyAddress][4]'),
			__('Address Line 6', 'mappress') => array($this->options->metaKeyAddress[5], 'mappress_options[metaKeyAddress][5]'),
			__('Latitude', 'mappress') => array($this->options->metaKeyLat, 'mappress_options[metaKeyLat]'),
			__('Longitude', 'mappress') => array($this->options->metaKeyLng, 'mappress_options[metaKeyLng]'),
			__('Icon', 'mappress') => array($this->options->metaKeyIconid, 'mappress_options[metaKeyIconid]'),
			__('Title', 'mappress') => array($this->options->metaKeyTitle, 'mappress_options[metaKeyTitle]'),
			__('Body', 'mappress') => array($this->options->metaKeyBody, 'mappress_options[metaKeyBody]'),
			__('Map Zoom', 'mappress') => array($this->options->metaKeyZoom, 'mappress_options[metaKeyZoom]')
		);

		$headers = array(__('Map', 'mappress'), __('Custom Field', 'mappress'));
		$rows = array();

		foreach($fields as $label => $field) {
			$rows[] = array($label, self::dropdown($meta_keys, $field[0], $field[1], array('none' => true)));
		}

		echo $this->table($headers, $rows);
		echo "<br/>";
		echo self::checkbox($this->options->metaSyncSave, 'mappress_options[metaSyncSave]', __('Overwrite existing maps when updating', 'mappress'));
	}

	function set_api_key() {
		echo "<input type='text' size='50' name='mappress_options[apiKey]' value='{$this->options->apiKey}' />";
		$link = "<a href='https://developers.google.com/maps/documentation/javascript/tutorial#api_key'>" . __('usage tracking', 'mappress') . "</a>";
		echo "<br/><i>" .  sprintf(__("API keys is needed only for premium services or %s", 'mappress'), $link) . "</i>";
	}

	function set_force_resize() {
		$from = "<input type='text' size='2' name='resize_from[width]' value='' />"
			. "x<input type='text' size='2' name='resize_from[height]' value='' /> ";
		$to = "<input type='text' size='2' name='resize_to[width]]' value='' />"
			. "x<input type='text' size='2' name='resize_to[height]]' value='' /> ";
		echo __('Permanently resize existing maps', 'mappress');
		echo ": <br/>";
		printf(__('from %s to %s', 'mappress'), $from, $to);
		echo "<input type='submit' name='force_resize' class='button' value='" . __('Force Resize', 'mappress') . "' />";
	}

	function get_rule_keys() {
		$keys = array();
		$tax_objects = get_taxonomies(array('public' => true), 'objects');
		foreach($tax_objects as $tax => $tax_object) {
			if (!in_array($tax, array('nav_menu', 'link_category', 'post_format')))
				$keys[$tax_object->name] = $tax_object->label;
		}
		return $keys;
	}

	static function ajax_get_rule_values() {
		$key = (isset($_REQUEST['key'])) ? $_REQUEST['key'] : null;
		Mappress::ajax_response('OK', self::get_rule_values($key));
	}

	static function get_rule_values($key) {
		if (!$key)
			return array();

		if ($key == 'post_type')
			$values = array_combine(Mappress::$options->postTypes, Mappress::$options->postTypes);

		else {
			$terms = get_terms($key, array('hide_empty' => false));
			$walker = new Mappress_Walker();
			$walk = $walker->walk($terms, 0, array('indent' => true));
			$values = (is_array($walk) && !empty($walk)) ? $walk : array();
		}

		// Add a blank entry
		$values = array('' => '&nbsp;') + $values;
		return $values;
	}
} // End class Mappress_Pro_Settings
?>