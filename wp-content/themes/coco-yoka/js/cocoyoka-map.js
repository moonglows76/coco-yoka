	var windowWidth = window.innerWidth;
	var geocoder;
	var map;
	var markers = [];
	var checkMarker = false;
	var setattrsBtn = [];
	var defaultPlaceholder = document.getElementById( 'address' ).getAttribute( 'placeholder' );

	function setCodeAddress() {
		addressValue = document.getElementById( 'address' ).value;
		searchPlace = document.getElementById( 'searchPlace' );
		if( markers.length === 0 && addressValue.trim() !== '' ) {
			searchPlace.setAttribute( 'onclick', 'codeAddress()' );
			document.getElementById( 'currentpos' ).removeAttribute( 'onclick' );
		}
	}

	function setCurrentPlace() {
		currentPlace = document.getElementById( 'currentpos' );
		currentPlace.setAttribute( 'onclick', 'getLocation()' );
	}

	function disableSearchPlace() {
		document.getElementById( 'address' ).setAttribute( 'placeholder', '現在位置取得中' );
		document.getElementById( 'address' ).setAttribute( 'disabled', 'disabled' );
	}

	function setDefaultPlacehoder() {
		document.getElementById( 'address' ).removeAttribute( 'disabled' );
		document.getElementById( 'address' ).setAttribute( 'placeholder', defaultPlaceholder );
	}

	function initialize( errorFlag ) {
		var markers = [];
	  geocoder = new google.maps.Geocoder();
	  document.getElementById( 'lat_data' ).setAttribute( 'disabled', 'disabled' );
	  //document.getElementById( 'lat_data' ).setAttribute( 'type', 'hidden' );
	  //document.getElementById( 'longi_data' ).setAttribute( 'type', 'hidden' );
	  document.getElementById( 'longi_data' ).setAttribute( 'disabled', 'disabled' );
	  //var latlng = new google.maps.LatLng(-34.397, 150.644);
	  var latlng_default = geocoder.geocode( { 'address': '鹿児島大学' }, function( results, status ) {
	    if ( status == google.maps.GeocoderStatus.OK ) {
	      map.setCenter( results[ 0 ].geometry.location );
	      map.setZoom( 12 );
	    } else {
	      alert( '場所が見つかりません。デフォルトの場所を再設定してください。' );
	    }
	  } );
	  if( errorFlag ) {
	  	var mapOptions = {
		    zoom: 8,
		    center: latlng_default,
		    zoomControl: true,
		    scrollwheel: false,
		    scaleControl: true
		  }
	  }
	  if( windowWidth >= 767 ) {
		  var mapOptions = {
		    zoom: 8,
		    center: latlng_default,
		    zoomControl: true,
		    scrollwheel: false,
		    scaleControl: true
		  }
		 } else {
		 	var mapOptions = {
		    zoom: 8,
		    center: latlng_default,
		    scrollwheel: false,
		    zoomControl: true
		  }
		 }
	  map = new google.maps.Map( document.getElementById( 'map-canvas' ), mapOptions );
	}

	function codeAddress() {
		if( markers.length === 0 ) {
		  var address = document.getElementById('address').value;
		  geocoder.geocode( { 'address': address}, function( results, status ) {
		    if ( address && status == google.maps.GeocoderStatus.OK ) {

		      map.setCenter( results[ 0 ].geometry.location );
		      map.setZoom( 15 );
		      var marker = new google.maps.Marker({
		      		map: map,
		          position: results[ 0 ].geometry.location,
		          draggable: true,
		          animation: google.maps.Animation.DROP,
		      });

					// set first lat longi data
					document.getElementById( 'lat_data' ).value = results[ 0 ].geometry.location[ 'A' ];
					document.getElementById( 'longi_data' ).value = results[ 0 ].geometry.location[ 'F' ];

		      // マーカー停止中はLat, Longiエリアの書き込み禁止
		      google.maps.event.addListener( marker, 'dragstart', function( ev ){
						enableLatLongiAarea();
					});
		      // マーカーのドロップ（ドラッグ終了）時のイベント
					google.maps.event.addListener( marker, 'dragend', function( ev ){
						// set first lat longi data
						document.getElementById( 'lat_data' ).value = ev.latLng.lat();
						document.getElementById( 'longi_data' ).value = ev.latLng.lng();
						disableLatLongiAarea();
					});

					document.getElementById( 'searchPlace' ).removeAttribute( 'onclick' );
					document.getElementById( 'redo' ).setAttribute( 'onclick', 'deleteMarkers()' );

		    } else if( address == '' ) {
		    	alert( '検索条件を入力してください。' );
		    }	else {
		      alert( 'お探しの場所は見つかりませんでした。' );
		      document.getElementById( 'address' ).value   = '';
	    		document.getElementById( 'lat_data' ).value  = '';
					document.getElementById( 'longi_data' ).value = '';
					document.getElementById( 'searchPlace' ).removeAttribute( 'onclick' );
	    		document.getElementById( 'redo' ).removeAttribute( 'onclick' );
		    }
		    if( document.getElementById( 'address' ).value !== '' ) {
		    	markers.push( marker );
		    }
		  });
			//checkMarker = true;
		} else {
			markers[ 0 ].setMap( null );
		}
	}

	// Delese marker :) this form able to post only one marker.
	function deleteMarkers() {
		if( markers.length > 0 ) {
	    //Loop through all the markers and remove
	    for ( var i = 0; i < markers.length; i++ ) {
	    	markers[ i ].setMap( null );
	    	document.getElementById( 'address' ).value   = '';
	    	document.getElementById( 'lat_data' ).value  = '';
				document.getElementById( 'longi_data' ).value = '';
	    }
	    markers = [];
	    document.getElementById( 'searchPlace' ).removeAttribute( 'onclick' );
	    document.getElementById( 'redo' ).removeAttribute( 'onclick' );
	    setCurrentPlace();
	    setDefaultPlacehoder();
	   }
	 }

	function getLocation() {
		// Try HTML5 geolocation
	  if( navigator.geolocation ) {
	    navigator.geolocation.getCurrentPosition( function( position ) {
	      var pos = new google.maps.LatLng( position.coords.latitude, position.coords.longitude );
	      console.log();
	      var infoComment = 'Location found using HTML5.<br /><strong>Share your recommended spot to the World!! :)</strong>'
	      var marker = new google.maps.Marker({
	      		map: map,
	          position: pos,
	          draggable: true,
	          animation: google.maps.Animation.DROP,
	      });
	      markers.push( marker );
	      //console.log(markers);

	      map.setCenter(pos);
	      map.setZoom(18);

	      disableSearchPlace();

	      // set first lat longi data
				document.getElementById( 'lat_data' ).value = pos[ 'A' ];
				document.getElementById( 'longi_data' ).value = pos[ 'F' ];

	      // マーカーのドロップ（ドラッグ終了）時のイベント
				google.maps.event.addListener( marker, 'dragend', function( ev ){
					// set first lat longi data
					document.getElementById( 'lat_data' ).value = ev.latLng.lat();
					document.getElementById( 'longi_data' ).value = ev.latLng.lng();
				});

				document.getElementById( 'currentpos' ).removeAttribute( 'onclick' );
				document.getElementById( 'redo' ).setAttribute( 'onclick', 'deleteMarkers()' );

	    }, function() {
	      handleNoGeolocation( true );
	    });
	  } else {
	    // Browser doesn't support Geolocation
	    handleNoGeolocation( false );
	  }
	}

	function handleNoGeolocation( errorFlag ) {
	  if ( errorFlag ) {
	    var content = '位置情報の取得に失敗しました。<br />やり直すボタンをクリックして<br />おすすめスポットを登録してください';
	    document.getElementById( 'redo' ).setAttribute( 'onclick', 'deleteMarkers(), setDefaultPlacehoder()' );
	  } else {
	    var content = 'お使いのブラウザは geolocation に対応していないようです。.';
	  }

	  var options = {
	    map: map,
	    position: new google.maps.LatLng( 31.571918, 130.54524000000004 ),
	    content: content
	  };

	  var infowindow = new google.maps.InfoWindow(options);
	  map.setCenter(options.position);
	}

	google.maps.event.addDomListener( window, 'load', initialize );