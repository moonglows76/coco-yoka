( function( $ ) {

  function setLabel() {
    // contact us form - change out optgroup labels
    // search for parent_ items
    var foundin = $('option:contains("optgroup-")');
    $.each(foundin, function(value) {
      var updated = $(this).val().replace("optgroup-","");
      // find all following elements until endoptgroup
      $(this).nextUntil('option:contains("endoptgroup")')
     .wrapAll('<optgroup label="'+ updated +'"></optgroup');
    });
    // remove placeholder options
    $('option:contains("optgroup-")').remove();
    $('option:contains("endoptgroup")').remove();
  }
  setLabel();

} )( jQuery );