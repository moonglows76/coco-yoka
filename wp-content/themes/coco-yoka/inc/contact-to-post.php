<?php

if( function_exists( 'wpcf7_add_shortcode' ) ) { wpcf7_add_shortcode( 'postdropdown', 'createbox', true ); }
	function createbox(){
	global $post;
	$output = "<select name='cursus' id='cursus' onchange='document.getElementById(\"cursus\").value=this.value;'>";
	$args = array(
		'type'                     => 'post',
		'child_of'                 => 0,
		'parent'                   => '',
		'orderby'                  => 'name',
		'order'                    => 'ASC',
		'hide_empty'               => 0,
		'hierarchical'             => 1,
		'exclude'                  => '',
		'include'                  => '',
		'number'                   => '',
		'taxonomy'                 => 'category',
		'pad_counts'               => false,
	);
	$categories = get_categories( $args );
	if( ! empty( $categories )  && ! is_wp_error( $categories ) ) {
		$flag = false;
		foreach ( ( array )$categories as $cat ) :
			if( $cat->parent == 0 ) {
				$title = 'optgroup-' . esc_attr( $cat->name );
			} else {
				$title = esc_attr( $cat->name );
			}
			$output .= "<option value='$title'> $title </option>";
			endforeach;
		$output .= "</select>";
	}
	return $output;
}


include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) ) {
	add_action( 'wpcf7_before_send_mail', 'contact_to_post' );
	function contact_to_post( $cf7 ) {
		if( isset( $_POST[ 'coco_shoptitle' ] ) )
		 $coco_shoptitle = $_POST[ 'coco_shoptitle' ];
		if(isset( $_POST[ 'coco_comment' ] ) )
			$coco_comment   = $_POST[ 'coco_comment' ];
		if( isset( $_POST[ 'coco_cat' ] ) )
			$coco_cat       = $_POST[ 'coco_cat' ];
		if( isset( $_POST[ 'coco_mail' ] ) )
			$coco_mail      = $_POST[ 'coco_mail' ];
		if( isset( $_POST[ 'coco_ido' ] ) )
			$coco_ido       = $_POST[ 'coco_ido' ];
		if( isset( $_POST[ 'coco_keido' ] ) )
			$coco_keido     = $_POST[ 'coco_keido' ];
		if( isset( $_POST[ 'cursus' ] ) ) {
			$cat_name       = $_POST[ 'cursus' ];
			$cat_id         = get_cat_id( esc_html( $cat_name ) );
		} else {
			$cat_id         = '';
		}

		$post = array(
			'post_title'    => esc_html( $coco_shoptitle ),
			'post_content'  => apply_filters( 'the_content', $coco_comment ),
			'post_status'   => 'draft',
			'post_type'     => 'post',
			'post_author'   => '1',
			'post_category' => array( intval( $cat_id ) ),
		);
		$id = wp_insert_post( $post );

		if( ! empty( $coco_shoptitle ) && ! is_wp_error( $coco_shoptitle ) )
			add_post_meta( $id, 'coco_shoptitle', esc_html( $coco_shoptitle ), true );
		if( ! empty( $coco_mail ) && ! is_wp_error( $coco_mail ) )
			add_post_meta( $id, 'coco_mail', sanitize_email( $coco_mail ), true );
		if( ! empty( $coco_ido ) )
			add_post_meta( $id, 'coco_ido', ( float )$coco_ido, true );
		if( ! empty( $coco_keido ) && ! is_wp_error( $coco_keido ) )
			add_post_meta( $id, 'coco_keido', ( float )$coco_keido, true );


		$dir = trailingslashit(  wpcf7_upload_dir( 'dir' ) . '/wpcf7_uploads' );
		$result = coco_list_files( $dir );

	  if( isset( $_FILES[ 'coco_image' ] ) ) {
	  	$source           = $result[ 0 ];
	  	$image_exif       = exif_read_data( $source  );
	  	//echo '<pre>';
	  	//print_r( $image_exif );
	  	//echo '<pre>';
	  	//if( ! empty( $image_exif ) && ! is_wp_error( $image_exif ) )
	  	//	$image_exif->Orientation = '';

			// upload file ( jpg )
			$image_name_datas = $_FILES[ 'coco_image' ];
			$image_name       = $image_name_datas[ 'name' ];
			$uploads_dir      = wpcf7_upload_tmp_dir();
			$uploads_dir      = trailingslashit( wpcf7_upload_tmp_dir() );
			$image_location   = trailingslashit( $uploads_dir ) . $image_name_datas[ 'name' ];

			$content          = file_get_contents( $source );
			$wud              = wp_upload_dir();
			$upload           = wp_upload_bits( $image_name, '', $content );
			$chemin_final     = $upload[ 'url' ];
			$custom_filename  = $upload[ 'file' ];

			require_once( ABSPATH . 'wp-admin/includes/admin.php' );
		  $wp_filetype      = wp_check_filetype( basename( $custom_filename ), null );
			$attachment       = array(
				'post_type'     => 'attachment',
				'post_mime_type'=> $wp_filetype[ 'type' ],
				'post_title'    => $coco_shoptitle,
			 	'post_content'  => '',
			 	'post_status'   => 'inherit',
		  );

		  $attach_id = wp_insert_attachment( $attachment, $custom_filename, $id );
		  //merty_fix_rotation( $custom_filename );

		  require_once( ABSPATH . 'wp-admin/includes/image.php' );
		  $attach_data = wp_generate_attachment_metadata( $attach_id, $custom_filename );
		  wp_update_attachment_metadata( $attach_id, $attach_data );
			update_post_meta( $id, '_thumbnail_id', $attach_id );
			add_post_meta( $id, 'coco_image', $image_name );
		}
	}

	function merty_attachment_uploaded( $id ) {

			$attachment = get_post( $id );
			echo '<pre>';
			print_r( $attachment );
			echo '</pre>';

			if ( 'image/jpeg' == $attachment->post_mime_type ) {
				merty_fix_rotation( $attachment->guid );
				$attachment_meta = wp_generate_attachment_metadata( $attachment->ID, str_replace( get_bloginfo('url'), ABSPATH, $attachment->guid ) );
				wp_update_attachment_metadata( $attachment->ID, $attachment_meta );
		    }
		}

	//function merty_fix_rotation( $source ) {
//
	//	$source = str_replace( get_bloginfo('url'), ABSPATH, $source );
	//	$sourceFile = explode( '/', $source );
	//	$filename = $sourceFile[5];
//
	//	$destination = $source;
	//	echo '<pre>';
	//	print_r( $source );
	//	echo '</pre>';
//
	//	$size = getimagesize( $source );
//
	//	$width = $size[0];
	//	$height = $size[1];
//
	//	$sourceImage = imagecreatefromjpeg( $source );
//
	//	$destinationImage = imagecreatetruecolor( $width, $height );
//
	//	imagecopyresampled( $destinationImage, $sourceImage, 0, 0, 0, 0, $width, $height, $width, $height );
//
	//	$exif = exif_read_data( $source );
//
	//	$ort = $exif['Orientation'];
//
	//	switch ( $ort ) {
//
	//		case 2:
	//			merty_flip_image( $dimg );
	//			break;
	//		case 3:
	//			$destinationImage = imagerotate( $destinationImage, 180, -1 );
	//			break;
	//		case 4:
	//			merty_flip_image( $dimg );
	//			break;
	//		case 5:
	//			merty_flip_image( $destinationImage );
	//			$destinationImage = imagerotate( $destinationImage, -90, -1 );
	//			break;
	//		case 6:
	//			$destinationImage = imagerotate( $destinationImage, -90, -1 );
	//			break;
	//		case 7:
	//			merty_flip_image( $destinationImage );
	//			$destinationImage = imagerotate( $destinationImage, -90, -1 );
	//			break;
	//		case 8:
	//			$destinationImage = imagerotate( $destinationImage, 90, -1 );
	//			break;
	//	}
//
	//	return imagejpeg( $destinationImage, $destination, 100 );
	//}
//
	//function merty_flip_image( &$image ) {
//
	//	$x = 0;
	//	$y = 0;
	//	$height = null;
	//	$width = null;
//
	//    if ( $width  < 1 )
	//    	$width  = imagesx( $image );
//
	//    if ( $height < 1 )
	//    	$height = imagesy( $image );
//
	//    if ( function_exists('imageistruecolor') && imageistruecolor( $image ) )
	//        $tmp = imagecreatetruecolor( 1, $height );
	//    else
	//        $tmp = imagecreate( 1, $height );
//
	//    $x2 = $x + $width - 1;
//
	//    for ( $i = (int)floor( ( $width - 1 ) / 2 ); $i >= 0; $i-- ) {
	//        imagecopy( $tmp, $image, 0, 0, $x2 - $i, $y, 1, $height );
	//        imagecopy( $image, $image, $x2 - $i, $y, $x + $i, $y, 1, $height );
	//        imagecopy( $image, $tmp, $x + $i,  $y, 0, 0, 1, $height );
	//    }
//
	//    imagedestroy( $tmp );
//
	//    return true;
	//}

	function coco_list_files( $dir ){
		$list = array();
		$files = scandir( $dir );
		foreach( $files as $file ){
			if( $file == '.' || $file == '..' || $file == '.htaccess' ){
				continue;
			} else if ( is_file( $dir . $file ) ){
				$list[] = $dir . $file;
			} else if( is_dir( $dir . $file ) ) {
				$list = array_merge( $list, coco_list_files($dir . $file . DIRECTORY_SEPARATOR ) );
			}
		}
		return $list;
	}
}

