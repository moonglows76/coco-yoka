<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package WB CoCo-yoka
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function coco_yoka_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'footer'    => 'page',
	) );
}
add_action( 'after_setup_theme', 'coco_yoka_jetpack_setup' );
