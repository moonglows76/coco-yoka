<?php
/**
 * The template for displaying pages
 *
 * for home page
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<div id="secMapArea">
			<?php echo do_shortcode('[mashup width="90%" height="400" alignment="center" border="0"]'); ?>
		</div>
		<div class="bt_submit"><a href="/entry"><img src="/wp-content/themes/coco-yoka/img/btn_submit.png" alt="スポットを投稿" /></a></div>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

	<div id="secfirstView_outer">
		<div id="secfirstView">
			<p class="secfirstView_lead">周辺のポイントを紹介しています！</p>
			<ul class="secfirstView_category">
			<li><a href="#"><img src="/wp-content/themes/coco-yoka/img/btn_category_lunch.png" alt="ランチ" /></a></li>
			<li><a href="#"><img src="/wp-content/themes/coco-yoka/img/btn_category_spot.png" alt="スポット" /></a></li>
			<li><a href="#"><img src="/wp-content/themes/coco-yoka/img/btn_category_access.png" alt="アクセス" /></a></li>
			</ul>
			<p class="secfirstView_btn"><a href="#">×</a></p>
		</div>
	</div>

	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script>
		$(function () {
			if(window.localStorage){
				var storage = localStorage;
				if(storage.getItem("flgfirstView") === null) {
					$("#secfirstView_outer").fadeIn(500);
					storage.setItem("flgfirstView",0);
				}
			}
			$(".secfirstView_btn a").on("click", function () {
				$("#secfirstView_outer").fadeOut(500);
			});
			$(document).click(function(event) { // (1)
			    if (!$.contains($("#secfirstView")[0], event.target)) { // (2)
			        $("#secfirstView_outer").fadeOut(500);
			    }
			});
		})
	</script>

<?php get_footer(); ?>
