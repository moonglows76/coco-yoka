<?php
/**
 * Load Google Maps custom :)
 */
add_action( 'wp_enqueue_scripts', 'cocoyoka_scripts', 20 );
function cocoyoka_scripts() {
	// remove parent style
	wp_deregister_style( 'twentyfifteen-style' );
	// add parent style after remove default style.
	wp_enqueue_style(
		'parent_css',
		get_template_directory_uri() . '/style.css',
		date( 'TmdHis', filemtime( get_stylesheet_directory() . '/style.css' ) )
	);
	// add child style :)
	wp_enqueue_style(
		'cocoyoka_css',
		get_stylesheet_directory_uri() . '/style.css',
		array( 'parent_css' ),
		date( 'TmdHis', filemtime( get_stylesheet_directory() . '/style.css' ) ),
		'all'
	);
	// Load the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'cocoyoka-ie', get_template_directory_uri() . '/css/ie.css', array( 'parent_css' ), '20141010' );
	wp_style_add_data( 'cocoyoka-ie', 'conditional', 'lt IE 9' );
	// Load the Internet Explorer 7 specific stylesheet.
	wp_enqueue_style( 'cocoyoka-ie7', get_template_directory_uri() . '/css/ie7.css', array( 'parent_css' ), '20141010' );
	wp_style_add_data( 'cocoyoka-ie7', 'conditional', 'lt IE 8' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( 'cocoyoka-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20141010' );
	}
	wp_localize_script( 'cocoyoka-script', 'screenReaderText', array(
		'expand'   => '<span class="screen-reader-text">' . __( 'expand child menu', 'twentyfifteen' ) . '</span>',
		'collapse' => '<span class="screen-reader-text">' . __( 'collapse child menu', 'twentyfifteen' ) . '</span>',
	) );
	// Load scripts for entry page( map encty form ) :)
	if( is_page( 'entry' ) ) {
		wp_enqueue_script(
			'google-map-js',
			'https://maps.googleapis.com/maps/api/js?v=3.exp',
			false,
			1,
			true
		);
		wp_enqueue_script(
			'parent-style',
			get_stylesheet_directory_uri() . '/js/contact-label.js',
			array( 'jquery', 'google-map-js' ),
			date( 'YmdHis', filemtime( get_stylesheet_directory() . '/js/contact-label.js' ) ),
			true
		);
		wp_enqueue_script(
			'cocoyoka-map-js',
			get_stylesheet_directory_uri() . '/js/cocoyoka-map.js',
			false,
			date( 'YmdHis', filemtime( get_stylesheet_directory() . '/js/cocoyoka-map.js' ) ),
			true
		);
	}
}

/**
 * Load Contact to Pst
 */
get_template_part( 'inc/contact-to-post' );


