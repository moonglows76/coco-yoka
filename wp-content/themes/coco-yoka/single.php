<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
 
get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">

		<?php
		// Start the loop.
//		while ( have_posts() ) : the_post();

			/*
			 * Include the post format-specific template for the content. If you want to
			 * use this in a child theme, then include a file called called content-___.php
			 * (where ___ is the post format) and that will be used instead.
			 */
//			get_template_part( 'content', get_post_format() );
		

			// If comments are open or we have at least one comment, load up the comment template.
//			if ( comments_open() || get_comments_number() ) :
//				comments_template();
//			endif;

			// Previous/next post navigation.
//			the_post_navigation( array(
//				'next_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Next', 'twentyfifteen' ) . '</span> ' .
//					'<span class="screen-reader-text">' . __( 'Next post:', 'twentyfifteen' ) . '</span> ' .
//					'<span class="post-title">%title</span>',
//				'prev_text' => '<span class="meta-nav" aria-hidden="true">' . __( 'Previous', 'twentyfifteen' ) . '</span> ' .
//					'<span class="screen-reader-text">' . __( 'Previous post:', 'twentyfifteen' ) . '</span> ' .
//					'<span class="post-title">%title</span>',
//			) );

		// End the loop.
//		endwhile;

	if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>

<div class="post">
	<article id="post-1" class="post-1 post type-post status-publish format-standard hentry category-1">                         
		<header class="entry-header">
        <h1 class="entry-title"><?php the_title(); ?></h1>
		</header>
        
        <div class="entry-content">
        <div class="entrylist-cat">><?php the_category(', ') ?></div>
		<!--<div class="tag"><?php the_tags('', ', '); ?></div>-->
        <div id="secMapArea"><?php echo do_shortcode('[mashup width="90%" height="400" alignment="center" border="0"]'); ?></div>
        
        
        <div class="coco_comment"><?php echo get_post_meta($post->ID , 'coco_comment' ,true); ?></div>
        
        
        <div class="coco_image"><?php if ( get_post_meta( get_the_ID(), 'thumb', true ) ) : ?>
    <a href="<?php the_permalink() ?>" rel="bookmark">
        <img class="thumb" src="<?php echo get_post_meta( get_the_ID(), 'thumb', true ) ?>" alt="<?php the_title(); ?>" /></a>
<?php endif; ?></div>


        <div class="coco_comment"><?php the_content(); ?></div>
        <?php if(has_post_thumbnail()) { echo the_post_thumbnail(); } ?>
		</div><!-- /.entry-content -->
	</article>
</div><!-- /.post -->
                         
	<?php endwhile; ?>
    
<div class="nav-below">
	<span class="nav-previous"><?php previous_post_link('%link', '前へ'); ?></span>
	<span class="nav-next"><?php next_post_link('%link', '次へ'); ?></span>
</div><!-- /.nav-below -->

	<?php else : ?>
                     
<h2 class="title">記事が見つかりませんでした。</h2>
	<p>検索で見つかるかもしれません。</p>
    <br /><?php get_search_form(); ?>

	<?php endif; ?>
    
	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
