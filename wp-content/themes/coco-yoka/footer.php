<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

	</div><!-- .site-content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<ul>
		    <li><a href="http://kagoshima.html5j.org/html5conference.2015/" target="_blank">HTML5 Conference 2015 in 鹿児島</a></li>
		    <li><a href="http://wordbench.org/groups/kagoshima/" target="_blank">WordBench鹿児島</a></li>
		  </ul>
		</div><!-- .site-info -->
	</footer><!-- .site-footer -->

</div><!-- .site -->

<?php wp_footer(); ?>

</body>
</html>
